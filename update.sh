#!/bin/bash
#Simple script to update Ubuntu then schedule a 3am reboot if necessary

sudo apt-get update
sudo apt-get dist-upgrade -y
sudo snap refresh
sudo apt-get autoremove -y

if [ -f /var/run/reboot-required ]; then
	echo "shutdown -r now" | sudo at 3:00
fi
